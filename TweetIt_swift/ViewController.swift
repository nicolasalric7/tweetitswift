//
//  ViewController.swift
//  TweetIt_swift
//
//  Created by iem on 01/12/2016.
//  Copyright © 2016 iem. All rights reserved.
//

import UIKit
import Social

class ViewController: UIViewController {

    let ACTIONS_COMP:Int = 0
    let FEELINGS_COMP:Int = 1
    
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var pickerviewMood: UIPickerView!
    
    var feelings = [String]()
    var actions = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        actions =  ["dors", "mange", "suis en cours", "galère", "cours", "poireaute", "veux mourir", "m'ennuie"]
        feelings = [";)", ":)", ":(", ":O", "8)", ":o", ":D", "mdr", "lol", ":|", "( ͡° ͜ʖ ͡°)", "¯\\_(ツ)_/¯", "[̲̅$̲̅(̲̅5̲̅)̲̅$̲̅]"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Delegates and data sources
    
   
    @IBAction func tweetIt(_ sender: UIButton) {
        var action: String
        var feeling : String
        var tweet : String
        action = actions[pickerviewMood.selectedRow(inComponent: ACTIONS_COMP)]
        feeling = feelings[pickerviewMood.selectedRow(inComponent: FEELINGS_COMP)]
        var text : String
        text = messageField.text!
        tweet = "Je \(action) \(feeling) \n \(text)"
        print(tweet);
    
        if (SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter))
        {
            let tweetSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweetSheet?.setInitialText(tweet)
            
            self.present(tweetSheet!, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "WARNING", message: "Aucun compte twitter n'est associé", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }

    
    
    
}
extension ViewController: UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(component == ACTIONS_COMP){
            return actions[row];
        }
        else{
            return feelings[row];
        }
    }
}

extension ViewController: UIPickerViewDataSource{
    func numberOfComponents(in: UIPickerView) -> Int{
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(component == ACTIONS_COMP){
            return actions.count;
        }
        else{
            return feelings.count;
        }
    }
}

